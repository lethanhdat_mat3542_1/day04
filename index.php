<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Programming Day02</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js">
    </script>
    <style>
        .bold {
            font-weight: bold;
        }

        .danger {
            color: red;
        }

        body {
            display: flex;
            justify-content: center;
            margin-top: 40px;
        }

        .wrapper {
            border: 1.5px solid #4475a2;
            padding: 30px 60px;
        }

        label {
            background-color: rgb(85, 159, 39);
            width: 150px;
            display: inline-block;
            line-height: 30px;
            padding-left: 10px;
            color: white;
            border: 1.5px solid #4475a2;
        }

        .input-box {
            margin-bottom: 10px;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .input-radio {
            margin-bottom: 10px;
        }

        .button-box {
            margin-top: 30px;
            display: flex;
            justify-content: center;
        }

        .input-username {
            border: 1px solid #4475a2;
            margin-left: 30px;
        }

        .select-box {
            margin-left: 30px;

        }

        .button {
            background-color: rgb(85, 159, 39);
            ;
            color: white;
            padding: 12px 38px;
            border: 1.5px solid #4475a2;
            border-radius: 7px;
        }

        .khoa-classname {
            margin-left: 30px;
        }

        .khoa-select {
            display: flex;
            justify-content: start;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <form action="index.php" method="POST">
            <?php
                function validateDate($date, $format = 'd/m/Y')
                {
                    $d = DateTime::createFromFormat($format, $date);
                    return $d && $d->format($format) == $date;
                }
            ?>
            <?php
            $gentleErr = $khoaErr = $hovatenErr = $bornErr = $bornFormat = $diachiErr = "";
            $gentle = $khoa = $hovaten = $born =  $diachi = "";
            if (isset($_POST['dangkysinhvien'])) {
                if (!isset($_POST["gender"])) {
                    $gentleErr = "<p class='danger bold'>Hãy chọn giới tính</p>";
                } else {
                    $gentle = $_POST["gender"];
                }

                if (empty($_POST["khoa"])) {
                    $khoaErr = "<p class='danger bold'>Hãy chọn phân khoa</p>";
                } else {
                    $khoa = $_POST["khoa"];
                }

                if (empty($_POST["hovaten"])) {
                    $hovatenErr = "<p class='danger bold'>Hãy nhập tên</p>";
                } else {
                    $hovaten = $_POST["hovaten"];
                }

                if (empty($_POST["born"])) {
                    $bornErr = "<p class='danger bold'>Hãy nhập ngày sinh</p>";
                } else {
                    $born = $_POST["born"];
                    if(!validateDate($born)) {
                        $bornFormat =  "<p class='danger bold'>Hãy nhập ngày sinh đúng định dạng</p>";
                    }
                }

                if (empty($_POST["diachi"])) {
                    $diachiErr = "<p class='danger bold'>Hãy nhập địa chỉ</p>";
                } else {
                    $diachi = $_POST["diachi"];
                }
            }
            echo $hovatenErr;
            echo $khoaErr;
            echo $gentleErr;
            echo $bornErr;
            echo $bornFormat;
            ?>
            <div class="input-box username-box">
                <label>Họ và tên <span class="danger">*</span></label>
                <input type="text" class="input-username" name="hovaten" value="<?php echo $hovaten; ?>">
            </div>
            <?php
            $arr_khoas = [
                "" => "",
                "MAT" => "Khoa học máy tính",
                "KDL" => "Khoa học dữ liệu"
            ];
            $gentleArr = [
                0 => "Nam",
                1 => "Nữ"
            ];
            $date_format = "dd/mm/yyyy";
            ?>
            <div class="input-radio">
                <label>Giới tính <span class="danger">*</span></label>
                <?php
                for ($x = 0; $x < count($gentleArr); $x++) {
                    $sex = $gentleArr[$x];
                    $arr_key = array_keys($gentleArr)[$x];
                    $selectedGender = isset($gentle) && $gentle == strval($arr_key)  ? "checked=checked" : "";
                    echo '<input ' . $selectedGender . '  class="select-box" type="radio" name="gender" value="' . $arr_key . '" />' . $sex . '';
                }
                ?>
            </div>
            <div class="input-box khoa-select">
                <label>Phân Khoa <span class="danger">*</span></label>
                <select class="khoa-classname " name="khoa">
                    <?php
                    foreach ($arr_khoas as $key => $arr_khoa) {
                        $selected = !empty($khoa) &&  $khoa === $key ? "selected=" . $key : "";
                        echo '<option ' . $selected . ' value="' . $key . '">' . $arr_khoa . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="input-box">
                <label>Ngày sinh <span class="danger">*</span></label>
                <input type="text" class="date input-username" placeholder="<?php echo $date_format; ?>" name="born" value="<?php echo $born; ?>" />
            </div>
            <div class="input-box">
                <label>Địa chỉ</label>
                <input type="text" class="input-username" name="diachi" value="<?php echo $diachi; ?>" />
            </div>
            <div class="button-box">
                <input type="submit" class="button" name="dangkysinhvien" value="Đăng Ký" />
            </div>
        </form>
    </div>

    <script type="text/javascript">
        $(".date").datepicker({
            format: "<?php echo $date_format; ?>",
        });
    </script>
</body>

</html>